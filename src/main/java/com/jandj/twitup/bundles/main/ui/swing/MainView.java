package com.jandj.twitup.bundles.main.ui.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.jandj.twitup.bundles.twit.ui.ITwitListView;

public final class MainView extends JPanel {

	private static final long serialVersionUID = -4393605399406898477L;

	private JComponent userPane;
	private JComponent searchPane;
	private JComponent logPane;
	private JComponent newTwitPane;
	private JComponent twitListPane;

	/**
	 * @deprecated Will be removed when other pane with be ready. Prefers to use
	 *             {@link MainView#MainView(JPanel, JPanel, JPanel, ITwitListView, JPanel)}
	 */
	@Deprecated
	public MainView() {
		this(new JPanel(), new JPanel(), new JPanel(), new JPanel(), new JPanel());
	}

	/**
	 * Create the view.
	 */
	public MainView(JComponent userPane, JComponent searchPane, JComponent logPane, JComponent twitListPane,
			JComponent newTwitPane) {
		this.userPane = userPane;
		this.searchPane = searchPane;
		this.logPane = logPane;
		this.twitListPane = twitListPane;
		this.newTwitPane = newTwitPane;

		setLayout(new GridBagLayout());
	}

	public void refresh() {
		
		/* Set panes */
		add(userPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 1, 1));
		add(searchPane, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 1, 1));
		add(logPane, new GridBagConstraints(2, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 1, 1));
		add(newTwitPane, new GridBagConstraints(0, 1, 3, 1, 1, 1, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 1, 1));
		add(twitListPane, new GridBagConstraints(0, 2, 3, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 1, 1));
		revalidate();
	}

	public void setUserPane(JComponent userPane) {
		this.userPane = userPane;
		refresh();
	}

	public void setSearchPane(JComponent searchPane) {
		this.searchPane = searchPane;
		refresh();
	}

	public void setLogPane(JComponent logPane) {
		this.logPane = logPane;
		refresh();
	}

	public void setTwitListPane(JComponent twitListPane) {
		this.twitListPane = twitListPane;
		refresh();
	}

	public void setNewTwitPane(JComponent newTwitPane) {
		this.newTwitPane = newTwitPane;
		refresh();
	}
}
