package com.jandj.twitup.bundles.main.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;

import com.jandj.twitup.ui.ImagePanel;

public class AboutView extends JFrame {

	private static final long serialVersionUID = -5380455364159373728L;
	private static final Logger LOGGER = Logger.getLogger(AboutView.class);
	
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public AboutView() {
		setTitle("About Twitup");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Thread.currentThread().getContextClassLoader().getResource("images/logo-64.png")));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setLayout(new BorderLayout(0, 0));

		addTitle();
		addCredits();
		addCopyright();
		addOkButton();
	}

	public static void showAbout() {
		(new AboutView()).setVisible(true);
	}

	private void addTitle() {
		JPanel logoTitlePanel = new JPanel();
		logoTitlePanel.setLayout(new BorderLayout());
		logoTitlePanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		JLabel title = createTitle();
		JPanel logo = createLogo();

		logoTitlePanel.add(logo, BorderLayout.CENTER);
		logoTitlePanel.add(title, BorderLayout.SOUTH);

		contentPanel.add(logoTitlePanel, BorderLayout.NORTH);
	}

	private JLabel createTitle() {
		JLabel title = new JLabel("TwitUp");
		title.setFont(new Font("Serif", Font.BOLD, 24));
		title.setHorizontalAlignment(JLabel.CENTER);

		return title;
	}

	private JPanel createLogo() {
		JPanel logo = null;
		try {
			logo = new ImagePanel(Thread.currentThread().getContextClassLoader().getResource("images/logo-64.png"));
			logo.setPreferredSize(new Dimension(64, 64));
		} catch (IOException e) {
			LOGGER.error("Cannot create the logo", e);
		}
		return logo;
	}

	private void addOkButton() {
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(e -> dispose());
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		
	}

	private void addCredits() {
		JTextPane credits = new JTextPane();
		credits.setBackground(null);
		credits.setEditable(false);
		credits.setText("Based on the Stéphane Lucas course.\n\nDesigned by :\nJimmy Tournemaine\nJean-Arthur Ousmane");
		contentPanel.add(credits, BorderLayout.CENTER);
	}

	private void addCopyright() {
		JLabel copyright = new JLabel("2018 © J&J Inc.");
		copyright.setHorizontalAlignment(JLabel.CENTER);
		contentPanel.add(copyright, BorderLayout.SOUTH);
	}
}
