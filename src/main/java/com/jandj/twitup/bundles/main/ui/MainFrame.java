package com.jandj.twitup.bundles.main.ui;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;

import com.jandj.twitup.core.Twitup;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 4552884355521071854L;
	private static final Logger LOGGER = Logger.getLogger(MainFrame.class);

	public MainFrame() {
		/* Set frame */
		setTitle("TwitUp!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Thread.currentThread().getContextClassLoader().getResource("images/logo-64.png")));

		/* Set frame components */
		setJMenuBar(createMenuBar());
	}
	
	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();

		JMenu twitUpMenu = createTwitUpMenu();
		if (null != twitUpMenu) {
			menuBar.add(twitUpMenu);
		}

		return menuBar;
	}

	private JMenu createTwitUpMenu() {

		JMenu mnTwitup = null;
		boolean aboutAndQuitSet = false;

		/* Mac handling */
		if (Twitup.isMac()) {
			try {
				Class.forName("com.jandj.twitup.ui.osx.MainViewMenuHandler");
				aboutAndQuitSet = true;
			} catch (ClassNotFoundException e) {
				LOGGER.error(e);
			}
		}

		if (!aboutAndQuitSet) {

			/* Set item: About */
			JMenuItem mntmAbout = new JMenuItem("About");
			mntmAbout.addActionListener(e -> AboutView.showAbout());

			/* Set item: Quit */
			JMenuItem mntmQuit = new JMenuItem("Quit");
			mntmQuit.addActionListener(e -> dispose());
			mntmQuit.setAccelerator(
					KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
			mntmQuit.setIcon(new ImageIcon(Toolkit.getDefaultToolkit()
					.getImage(Thread.currentThread().getContextClassLoader().getResource("images/exitIcon_20.png"))));
			/* Set menu */
			mnTwitup = new JMenu("TwitUp");
			mnTwitup.add(mntmAbout);
			mnTwitup.add(mntmQuit);
		}
		return mnTwitup;
	}

	
}
