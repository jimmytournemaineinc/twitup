package com.jandj.twitup.bundles.main.ui.fx;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import com.jandj.twitup.bundles.twit.ui.ITwitListView;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;

public final class MainView extends JFXPanel {

	private static final long serialVersionUID = -4393605399406898477L;

	private Node userPane;
	private Node searchPane;
	private Node logPane;
	private Node newTwitPane;
	private Node twitListPane;

	/**
	 * @deprecated Will be removed when other pane with be ready. Prefers to use
	 *             {@link MainView#MainView(JPanel, JPanel, JPanel, ITwitListView, JPanel)}
	 */
	@Deprecated
	public MainView() {
		this(new GridPane(), new GridPane(), new GridPane(), new GridPane(), new GridPane());
	}

	/**
	 * Create the view.
	 */
	public MainView(Node userPane, Node searchPane, Node logPane, Node twitListPane, Node newTwitPane) {
		this.userPane = userPane;
		this.searchPane = searchPane;
		this.logPane = logPane;
		this.twitListPane = twitListPane;
		this.newTwitPane = newTwitPane;

		setLayout(new GridBagLayout());
	}

	public void init() {

		GridPane gridPane = new GridPane();
		
		gridPane.setVgap(5);
		gridPane.add(twitListPane, 0, 0);
		gridPane.add(newTwitPane, 0, 1);

		Platform.runLater(() -> {
			Scene scene = new Scene(gridPane, 350, 350);
			setScene(scene);
		});
	}
}
