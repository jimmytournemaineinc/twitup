package com.jandj.twitup.bundles.main.control;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.jandj.twitup.bundles.main.ui.MainFrame;
import com.jandj.twitup.bundles.main.ui.swing.MainView;
import com.jandj.twitup.bundles.twit.control.ITwitController;
import com.jandj.twitup.bundles.twit.control.TwitController;
import com.jandj.twitup.bundles.user.control.IUserController;
import com.jandj.twitup.bundles.user.control.SecurityController;
import com.jandj.twitup.bundles.user.control.UserController;
import com.jandj.twitup.bundles.user.event.LoginEvent;
import com.jandj.twitup.bundles.user.event.LogoutEvent;
import com.jandj.twitup.bundles.user.event.SearchEvent;
import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.listener.ISearchUserListener;
import com.jandj.twitup.core.Configuration;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.model.User;
import com.jandj.twitup.ui.UIMode;

import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 * Controller of the entire application.
 * 
 * @author Jimmy Tournemaine
 */
public class MainController implements ILogUserListener, ISearchUserListener {

	/** The logger. */
	private static final Logger LOGGER = Logger.getLogger(MainController.class);

	/** The entity manager. */
	protected EntityManager em;

	/** The database handler. */
	protected IDatabase db;

	/** The logged user. */
	protected User user;

	/** The frame containing the app. */
	protected MainFrame view;

	/** Security business controller. */
	protected SecurityController securityCtrl;

	/** User business controller. */
	protected IUserController userCtrl;

	/** Twit business controller. */
	protected ITwitController twitCtrl;

	/**
	 * Instantiate the main controller.
	 * 
	 * @param database
	 *            The database connection.
	 * @param manager
	 *            The entity manager.
	 */
	public MainController(IDatabase database, EntityManager manager) {

		db = database;
		em = manager;
		view = new MainFrame();

		/* Create controllers */
		LOGGER.debug("Initialize business controllers.");
		twitCtrl = new TwitController(db, em);
		securityCtrl = new SecurityController(em, db);
		userCtrl = new UserController(db, em);

		/* Add listeners */
		LOGGER.debug("Add control listeners");
		securityCtrl.addLogListener(this);
		securityCtrl.addLogListener(twitCtrl.getNewTwitCtrl());
		userCtrl.addSearchListener(this);

		displayLoginView();
		view.setVisible(true);
	}

	protected void displayLoginView() {
		LOGGER.debug("Displaying the security view");
		view.setContentPane(securityCtrl.getSecurityView());
		view.revalidate();
	}

	protected void displayProfileView(User u) {
		LOGGER.debug("Displaying the profile view");
		userCtrl.setProfileUser(u, false);
		view.setContentPane((JPanel) userCtrl.getProfileView());
		view.revalidate();
	}

	protected void displayMainView() {
		LOGGER.debug("Displaying the main view.");

		JComponent mv = null;
		
		if (UIMode.SWING == Configuration.getInstance().getUIMode()) {
			MainView swingView = new MainView();
			swingView.setTwitListPane((JComponent) twitCtrl.getTwitListView());
			swingView.setNewTwitPane((JComponent) twitCtrl.getNewTwitView());
			swingView.setLogPane((JComponent) securityCtrl.getLogoutView());
			swingView.setSearchPane((JComponent) userCtrl.getSearchView());

			mv = swingView;
		} else {
			mv = new com.jandj.twitup.bundles.main.ui.fx.MainView(
					new GridPane(), new GridPane(), new GridPane(), (Node) twitCtrl.getTwitListView(), new GridPane());
		}

		view.setContentPane(mv);
		view.revalidate();
	}

	@Override
	public void loggedIn(LoginEvent e) {
		user = e.getUser();
		displayMainView();
	}

	@Override
	public void loggedOut(LogoutEvent e) {
		user = null;
		displayLoginView();
	}

	@Override
	public void search(SearchEvent e) {
		displayProfileView(e.getUser());
	}

}
