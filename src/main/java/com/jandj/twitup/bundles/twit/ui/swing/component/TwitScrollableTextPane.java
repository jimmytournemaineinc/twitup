package com.jandj.twitup.bundles.twit.ui.swing.component;

import javax.swing.JScrollPane;
import javax.swing.border.Border;

public class TwitScrollableTextPane extends JScrollPane {

	private static final long serialVersionUID = 1772302962737127734L;
	TwitTextPane twitTextPane;
	
	public TwitScrollableTextPane() {
		twitTextPane = new TwitTextPane();
		init();
	}

	public TwitScrollableTextPane(String text) {
		twitTextPane = new TwitTextPane(text);
		init();
	}
	
	protected void init() {
		Border paneBorder = twitTextPane.getBorder();
		twitTextPane.setBorder(null);
		
		setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
		setBorder(paneBorder);
		setViewportView(twitTextPane);
	}

	public void setText(String twitText) {
		twitTextPane.setText(twitText);
		revalidate();
	}
	
	public String getText() {
		return twitTextPane.getText();
	}
}
