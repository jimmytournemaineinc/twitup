package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.model.TwitList;
import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.IDatabaseObserver;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.datamodel.model.User;

public class TwitListController implements ITwitListController, IDatabaseObserver {

	protected ITwitListView view;
	protected TwitList model;
	
	public TwitListController(IDatabase db) {
		model = new TwitList();
		db.addObserver(this);
	}

	@Override
	public void setView(ITwitListView twitListView) {
		model.removeListener(view);
		
		view = twitListView;
		model.addListener(view);
	}

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		model.addTwit(addedTwit);
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		model.removeTwit(deletedTwit);
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		model.editTwit(modifiedTwit);
	}

	@Override
	public void notifyUserAdded(User addedUser) {
		// NOT USED
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		// NOT USED
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		// NOT USED
	}

}
