package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.ui.INewTwitView;
import com.jandj.twitup.bundles.user.event.LoginEvent;
import com.jandj.twitup.bundles.user.event.LogoutEvent;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.datamodel.model.User;

/**
 * Controller class for the twit creation.
 * 
 * @author jimmytournemaine
 */
public class NewTwitController implements INewTwitController {

	protected INewTwitView view;
	protected EntityManager em;
	protected User user;

	public NewTwitController(EntityManager em) {
		this.em = em;
	}

	@Override
	public void twitSubmitted() {
		
		if(user == null) {
			throw new NullPointerException("No user is logged.");
		}
		
		Twit twit = new Twit(user, view.getTwitText());
		em.sendTwit(twit);
	}

	public INewTwitView getView() {
		return getView();
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void setView(INewTwitView newTwitView) {
		this.view = newTwitView;
	}

	@Override
	public void loggedIn(LoginEvent e) {
		setUser(e.getUser());
	}

	@Override
	public void loggedOut(LogoutEvent e) {
		setUser(null);
	}

}
