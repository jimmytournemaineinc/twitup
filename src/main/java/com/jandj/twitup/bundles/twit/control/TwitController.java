package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.model.TwitList;
import com.jandj.twitup.bundles.twit.ui.INewTwitView;
import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.bundles.twit.ui.swing.NewTwitView;
import com.jandj.twitup.bundles.twit.ui.swing.TwitListView;
import com.jandj.twitup.core.Configuration;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.ui.UIMode;

import javafx.scene.layout.GridPane;

/**
 * Business controller for twits.
 * 
 * @author Jimmy Tournemaine
 */
public class TwitController implements ITwitController {

	protected INewTwitController newTwitCtrl;
	protected INewTwitView newTwitView;

	protected ITwitListController twitListCtrl;
	protected ITwitListView twitListView;
	protected TwitList twitList;

	public TwitController(IDatabase db, EntityManager em) {
		newTwitCtrl = new NewTwitController(em);
		twitListCtrl = new TwitListController(db);
		twitList = new TwitList();
		
		if (UIMode.SWING == Configuration.getInstance().getUIMode()) {
			newTwitView = new NewTwitView(newTwitCtrl, "What's new ?");
			twitListView = new TwitListView();
		} else {
			newTwitView = (INewTwitView) new GridPane();
			twitListView = new com.jandj.twitup.bundles.twit.ui.jfx.TwitListView();
		}

		newTwitCtrl.setView(newTwitView);
		twitListCtrl.setView(twitListView);
	}

	@Override
	public INewTwitController getNewTwitCtrl() {
		return newTwitCtrl;
	}

	@Override
	public INewTwitView getNewTwitView() {
		return newTwitView;
	}

	@Override
	public ITwitListController getTwitListCtrl() {
		return twitListCtrl;
	}

	@Override
	public ITwitListView getTwitListView() {
		return twitListView;
	}
}
