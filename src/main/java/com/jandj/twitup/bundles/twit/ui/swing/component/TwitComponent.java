package com.jandj.twitup.bundles.twit.ui.swing.component;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jandj.twitup.common.Constants;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.ui.ImagePanel;

public class TwitComponent extends JPanel {

	private static final long serialVersionUID = -6595256656335651349L;
	private static final Logger LOGGER = Logger.getLogger(TwitComponent.class);

	protected static final String DEFAULT_AVATAR = "images/default-avatar.png";

	private Twit twit;

	public TwitComponent(Twit twit) {

		this.twit = twit;

		String username = twit.getTwiter().getName();
		String userTag = twit.getTwiter().getUserTag();
		long twittedAt = twit.getEmissionDate();
		String text = twit.getText();
		String avatarPath = twit.getTwiter().getAvatarPath();

		setLayout(new GridBagLayout());

		JPanel avatarPanel = createAvatarPanel(avatarPath);
		JPanel usernamePanel = createUsernamePanel(username);
		JPanel userTagPanel = createUserTagPanel(userTag);
		JPanel twittedAtPanel = createDatePanel(twittedAt);
		JPanel twitTextPanel = createTwitTextPanel(text);

		add(avatarPanel, new GridBagConstraints(0, 0, 1, 2, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 1, 1));
		add(twitTextPanel, new GridBagConstraints(1, 1, 3, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 1, 1));
		add(twittedAtPanel, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 1, 1));
		add(userTagPanel, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 1, 1));
		add(usernamePanel, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 1, 1));

	}

	private JPanel createTwitTextPanel(String text) {

		/* Text pane containing the twit */
		TwitTextPane textPane = new TwitTextPane(text);
		textPane.setEditable(false);

		/* Create the panel */
		JPanel twitTextPanel = new JPanel();
		twitTextPanel.setLayout(new BorderLayout());
		twitTextPanel.add(textPane, BorderLayout.CENTER);

		return twitTextPanel;
	}

	private JPanel createDatePanel(long twittedTimestamp) {

		/* Display the date */
		JLabel twittedAtLabel = new JLabel(
				(new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(new Date(twittedTimestamp)));

		/* Set panel */
		JPanel datePanel = new JPanel();
		datePanel.setLayout(new GridBagLayout());
		datePanel.add(twittedAtLabel);

		return datePanel;
	}

	private JPanel createUserTagPanel(String userTag) {

		/* Add label */
		JLabel tagLabel = new JLabel(Constants.USER_TAG_DELIMITER + userTag);

		/* Set panel */
		JPanel userTagPanel = new JPanel();
		userTagPanel.setLayout(new GridBagLayout());
		userTagPanel.add(tagLabel);

		return userTagPanel;
	}

	private JPanel createUsernamePanel(String username) {

		/* Add label */
		JLabel usernameLabel = new JLabel(username);

		/* Set Panel */
		JPanel usernamePanel = new JPanel();
		usernamePanel.setLayout(new GridBagLayout());
		usernamePanel.add(usernameLabel);

		return usernamePanel;
	}

	private JPanel createAvatarPanel(String avatarPath) {

		JPanel avatarPanel = null;
		if(StringUtils.isBlank(avatarPath)) {
			avatarPanel = getDefaultAvatar();
		} else {
			try {
				avatarPanel = new ImagePanel(avatarPath);
			} catch (IOException e) {
				LOGGER.error("Cannot read the avatar", e);
				avatarPanel = getDefaultAvatar();
			}
		}
		
		return avatarPanel;
	}

	private JPanel getDefaultAvatar() {
		JPanel avatarPanel = null;
		try {
			avatarPanel = new ImagePanel(DEFAULT_AVATAR);
		} catch (IOException e) {
			LOGGER.error("Cannot read the default image", e);
			avatarPanel = new JPanel();
		}
		return avatarPanel;
	}

	@Override
	public int hashCode() {
		return twit.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o instanceof TwitComponent) {
			return twit.equals(((TwitComponent) o).twit);
		}
		return false;
	}

}
