package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.control.Controller;

@FunctionalInterface
public interface ITwitListController extends Controller {

	/**
	 * Set the current view.
	 * 
	 * @param twitListView
	 */
	public void setView(ITwitListView twitListView);

}
