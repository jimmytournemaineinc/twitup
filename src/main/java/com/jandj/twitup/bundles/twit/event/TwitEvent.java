package com.jandj.twitup.bundles.twit.event;

import com.jandj.twitup.datamodel.model.Twit;

public abstract class TwitEvent {

	private Twit twit;

	public TwitEvent(Twit twit) {
		super();
		this.twit = twit;
	}

	public Twit getTwit() {
		return twit;
	}

	public void setTwit(Twit twit) {
		this.twit = twit;
	}
	
	
	
}
