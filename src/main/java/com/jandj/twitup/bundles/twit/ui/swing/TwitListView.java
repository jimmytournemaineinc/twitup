package com.jandj.twitup.bundles.twit.ui.swing;

import java.awt.GridLayout;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.JPanel;

import com.jandj.twitup.bundles.twit.control.ITwitListController;
import com.jandj.twitup.bundles.twit.event.TwitAddedEvent;
import com.jandj.twitup.bundles.twit.event.TwitDeletedEvent;
import com.jandj.twitup.bundles.twit.event.TwitEditedEvent;
import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.bundles.twit.ui.swing.component.TwitComponent;
import com.jandj.twitup.datamodel.model.Twit;

public class TwitListView extends JPanel implements ITwitListView {
	
	private static final long serialVersionUID = -913733100838191606L;
	
	protected transient ITwitListController controller;
	
	protected transient SortedSet<TwitWrapper> twits;
	
	public TwitListView() {
		twits = new TreeSet<>();
		
		setLayout(new GridLayout(0, 1));
	}
	
	protected void refresh() {
		removeAll();
		for(TwitWrapper tw : twits) {
			add(new TwitComponent(tw.getTwit()));
		}
		revalidate();
	}

	@Override
	public void twitAdded(TwitAddedEvent event) {
		twits.add(new TwitWrapper(event.getTwit()));
		refresh();
	}

	@Override
	public void twitEdited(TwitEditedEvent event) {
		TwitWrapper tw = new TwitWrapper(event.getTwit());
		twits.remove(tw);
		
		twits.add(new TwitWrapper(event.getTwit()));
		refresh();
	}

	@Override
	public void twitDeleted(TwitDeletedEvent event) {
		twits.remove(new TwitWrapper(event.getTwit()));
		refresh();
	}
	

	class TwitWrapper implements Comparable<TwitWrapper> {

		private Twit twit;
		
		public TwitWrapper(Twit twit) {
			this.twit = twit;
		}		

		@Override
		public int compareTo(TwitWrapper t) {
			if(twit.getEmissionDate() < t.getTwit().getEmissionDate()) {
				return 1;
			}
			if (twit.getEmissionDate() > t.getTwit().getEmissionDate()) {
				return -1;
			}
			return 0;
		}
		
		@Override
		public int hashCode() {
			return twit.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			if(null == o) {
				return false;
			}
			if(!(o instanceof TwitWrapper)) {
				return false;
			}
			return twit.equals(((TwitWrapper)o).getTwit());
		}

		public Twit getTwit() {
			return twit;
		}
		
	}
	
}
