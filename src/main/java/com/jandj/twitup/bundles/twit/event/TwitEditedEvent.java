package com.jandj.twitup.bundles.twit.event;

import com.jandj.twitup.datamodel.model.Twit;

public class TwitEditedEvent extends TwitEvent {

	public TwitEditedEvent(Twit twit) {
		super(twit);
	}

}
