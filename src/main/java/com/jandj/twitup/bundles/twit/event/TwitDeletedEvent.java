package com.jandj.twitup.bundles.twit.event;

import com.jandj.twitup.datamodel.model.Twit;

public class TwitDeletedEvent extends TwitEvent {

	public TwitDeletedEvent(Twit twit) {
		super(twit);
	}

}
