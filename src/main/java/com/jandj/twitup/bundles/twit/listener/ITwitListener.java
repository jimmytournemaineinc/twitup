package com.jandj.twitup.bundles.twit.listener;

import com.jandj.twitup.bundles.twit.event.TwitAddedEvent;
import com.jandj.twitup.bundles.twit.event.TwitDeletedEvent;
import com.jandj.twitup.bundles.twit.event.TwitEditedEvent;
import com.jandj.twitup.bundles.twit.model.TwitList;

/**
 * Define a listener of a twit list events.
 * 
 * @see {@link TwitList}
 * 
 * @author Jimmy Tournemaine
 */
public interface ITwitListener {

	/**
	 * A twit has been added to the twit list.
	 * 
	 * @param event
	 */
	public void twitAdded(TwitAddedEvent event);
	
	/**
	 * A twit has been edited in the twit list.
	 * 
	 * @param event
	 */
	public void twitEdited(TwitEditedEvent event);
	
	/**
	 * A twit has been deleted from the twit list.
	 * 
	 * @param event
	 */
	public void twitDeleted(TwitDeletedEvent event);
	
}
