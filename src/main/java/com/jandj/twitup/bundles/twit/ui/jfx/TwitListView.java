package com.jandj.twitup.bundles.twit.ui.jfx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jandj.twitup.bundles.twit.event.TwitAddedEvent;
import com.jandj.twitup.bundles.twit.event.TwitDeletedEvent;
import com.jandj.twitup.bundles.twit.event.TwitEditedEvent;
import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.bundles.twit.ui.jfx.component.TwitComponent;
import com.jandj.twitup.datamodel.model.Twit;

import javafx.application.Platform;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class TwitListView extends Pane implements ITwitListView {

	protected Map<Twit, TwitComponent> twitMap = new HashMap<>();
	protected List<Twit> twits = new ArrayList<>();

	protected ScrollPane scrollPane;

	protected GridPane contentPane;

	public TwitListView() {
		initComponent();
	}

	protected void initComponent() {
		contentPane = new GridPane();
		contentPane.setStyle("-fx-border-color: red;");
		contentPane.setPrefSize(370, 320);
		scrollPane = new ScrollPane(contentPane);
		scrollPane.setMinSize(390, 320);
		scrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);

		this.getChildren().add(scrollPane);
	}

	public synchronized void twitListHasChanged(List<Twit> twits) {
		List<TwitComponent> newTwitComponents = new ArrayList<>();
		for (Twit twit : twits) {

			TwitComponent component = twitMap.get(twit);

			// Nouveau twit
			if (component == null) {
				TwitComponent newTwitComponent = this.createTwitComponent(twit);
				this.addTwitComponent(twit, newTwitComponent);
				newTwitComponents.add(newTwitComponent);
			}
		}

		List<TwitComponent> deletedTwitComponents = new ArrayList<>();
		List<Twit> toRemove = new ArrayList<>();
		for (Entry<Twit,TwitComponent> entry : twitMap.entrySet()) {
			Twit oldTwit = entry.getKey();
			if (!twits.contains(oldTwit)) {
				TwitComponent oldTwitComponent = entry.getValue();
				if (oldTwitComponent != null) {
					deletedTwitComponents.add(oldTwitComponent);
				}
				toRemove.add(oldTwit);
			}
		}
		for (Twit remove : toRemove) {
			twitMap.remove(remove);
		}
		(new Thread(() -> updateTwitsComponents(deletedTwitComponents, newTwitComponents, twits))).start();
	}

	protected void updateTwitsComponents(List<TwitComponent> deletedTwitComponents,
			List<TwitComponent> newTwitComponents, List<Twit> allTwitToShow) {
		Platform.runLater(() -> {

			// Ajout des animations de disparition
			for (TwitComponent oldTwitComponent : deletedTwitComponents) {
				oldTwitComponent.hideTwit();
			}

			// Placement des composants ‡ ajouter (en conservant la liste triÈe)
			replaceTwit(allTwitToShow);

			// Ajout des animations d'apparition
			for (TwitComponent newTwitComponent : newTwitComponents) {
				newTwitComponent.showTwit();
			}
		});
	}

	private void replaceTwit(List<Twit> twits) {

		int posY = 0;

		for (Twit twit : twits) {
			TwitComponent component = twitMap.get(twit);

			if (component != null) {
				GridPane.setConstraints(component, 0, posY);
				posY++;
			}
		}
	}

	protected void addTwitComponent(Twit twit, TwitComponent component) {
		twitMap.put(twit, component);
		Platform.runLater(() -> {
			contentPane.add(component, 0, 0);
			GridPane.setFillWidth(component, true);
		});
	}

	protected TwitComponent createTwitComponent(Twit twit) {
		TwitComponent mockTwitComponent = new TwitComponent(twit);
		mockTwitComponent.setVisible(false);

		return mockTwitComponent;
	}

	@Override
	public void twitAdded(TwitAddedEvent event) {
		twits.add(event.getTwit());
		twitListHasChanged(twits);
	}

	@Override
	public void twitEdited(TwitEditedEvent event) {
		int index = twits.indexOf(event.getTwit());
		twits.set(index, event.getTwit());
		twitListHasChanged(twits);
	}

	@Override
	public void twitDeleted(TwitDeletedEvent event) {
		twitListHasChanged(twits);
	}
}
