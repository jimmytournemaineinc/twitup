package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.ui.INewTwitView;
import com.jandj.twitup.bundles.twit.ui.ITwitListView;
import com.jandj.twitup.control.Controller;

/**
 * Control the twit domain
 * 
 * @author Jimmy Tournemaine
 */
public interface ITwitController extends Controller {

	/**
	 * Get The controller of the twit list.
	 * @return The twit list Controller.
	 */
	public ITwitListController getTwitListCtrl();
	
	/**
	 * The the twit list view.
	 * 
	 * @return the twit list view. 
	 */
	public ITwitListView getTwitListView();

	/**
	 * Get the controller of the twit list.
	 * @return The twit list controller.
	 */
	public INewTwitController getNewTwitCtrl();

	/**
	 * Get the view of the create twit feature.
	 * @return The new twit view.
	 */
	public INewTwitView getNewTwitView();
}
