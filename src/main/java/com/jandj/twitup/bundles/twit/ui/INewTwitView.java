package com.jandj.twitup.bundles.twit.ui;

import com.jandj.twitup.ui.View;

@FunctionalInterface
public interface INewTwitView extends View {

	/**
	 * Get the current text in the view.
	 * 
	 * @return The current text of the twit.
	 */
	public String getTwitText();
}
