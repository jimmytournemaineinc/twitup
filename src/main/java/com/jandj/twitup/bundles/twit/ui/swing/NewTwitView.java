package com.jandj.twitup.bundles.twit.ui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jandj.twitup.bundles.twit.control.INewTwitController;
import com.jandj.twitup.bundles.twit.ui.INewTwitView;
import com.jandj.twitup.bundles.twit.ui.swing.component.TwitScrollableTextPane;

/**
 * View of the new twit writing.
 * 
 * @author Jimmy Tournemaine
 */
public class NewTwitView extends JPanel implements INewTwitView {

	private static final long serialVersionUID = -3133167635900292082L;

	public static final String CANCEL_BUTTON = "cancel-button";
	public static final String SUBMIT_BUTTON = "submit-button";
	
	protected TwitScrollableTextPane twitPane;
	protected transient INewTwitController controller;

	/**
	 * Build the view.
	 */
	public NewTwitView(INewTwitController newTwitCtrl) {
		this(newTwitCtrl, "");
	}
	
	/**
	 * Build the view with a placeholder.
	 */
	public NewTwitView(INewTwitController controller, String defaultText) {
		this.controller = controller;
		
		/* Set components */
		twitPane = createTwitPane(defaultText);
		JPanel controlsPanel = createControls();

		/* Set the panel */
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		add(twitPane, BorderLayout.CENTER);
		add(controlsPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Create the twit text pane
	 */
	protected TwitScrollableTextPane createTwitPane(String defaultText) {
		return new TwitScrollableTextPane(defaultText);
	}
	
	/**
	 * Create the view controls
	 */
	protected JPanel createControls() {
		
		JButton twitButton = new JButton("Twit");
		twitButton.setName(SUBMIT_BUTTON);
		twitButton.addActionListener(e -> controller.twitSubmitted());

		JPanel controlsPanel = new JPanel();
		controlsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		controlsPanel.add(twitButton);
		return controlsPanel;
	}
	
	@Override
	public String getTwitText() {
		return twitPane.getText();
	}

}
