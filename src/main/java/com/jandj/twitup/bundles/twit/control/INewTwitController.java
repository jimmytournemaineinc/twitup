package com.jandj.twitup.bundles.twit.control;

import com.jandj.twitup.bundles.twit.ui.INewTwitView;
import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.control.Controller;

/**
 * Controller for twit creation
 * 
 * @author Jimmy Tournemaine
 */
public interface INewTwitController extends Controller, ILogUserListener {

	/**
	 * Send a new twit.
	 */
	public void twitSubmitted();

	/**
	 * Set the current view.
	 * 
	 * @param newTwitView
	 */
	public void setView(INewTwitView newTwitView);
}
