package com.jandj.twitup.bundles.twit.event;

import com.jandj.twitup.datamodel.model.Twit;

public class TwitAddedEvent extends TwitEvent {

	public TwitAddedEvent(Twit twit) {
		super(twit);
	}

}
