package com.jandj.twitup.bundles.twit.model;

import java.util.ArrayList;
import java.util.List;

import com.jandj.twitup.bundles.twit.event.TwitAddedEvent;
import com.jandj.twitup.bundles.twit.event.TwitDeletedEvent;
import com.jandj.twitup.bundles.twit.event.TwitEditedEvent;
import com.jandj.twitup.bundles.twit.listener.ITwitListener;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.events.Listenable;

public class TwitList implements Listenable<ITwitListener> {

	protected List<Twit> twits;
	protected List<ITwitListener> listeners;

	public TwitList() {
		this(new ArrayList<>());
	}

	public TwitList(List<Twit> twits) {
		this.twits = twits;
		this.listeners = new ArrayList<>();
	}

	public void addTwit(Twit twit) {
		this.twits.add(twit);
		listeners.forEach(listener -> listener.twitAdded(new TwitAddedEvent(twit)));
	}

	public void removeTwit(Twit twit) {
		this.twits.remove(twit);
		listeners.forEach(listener -> listener.twitDeleted(new TwitDeletedEvent(twit)));
	}
	
	public void editTwit(Twit twit) {
		listeners.forEach(listener -> listener.twitEdited(new TwitEditedEvent(twit)));
	}

	@Override
	public void addListener(ITwitListener listener) {
		listeners.add(listener);
		
		/* Notify with all twits */
		this.twits.forEach(twit -> listener.twitAdded(new TwitAddedEvent(twit)));
	}

	@Override
	public void removeListener(ITwitListener listener) {
		listeners.remove(listener);
	}
}
