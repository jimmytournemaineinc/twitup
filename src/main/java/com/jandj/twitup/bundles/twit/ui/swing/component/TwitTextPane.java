package com.jandj.twitup.bundles.twit.ui.swing.component;

import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

import com.jandj.twitup.common.Constants;

public class TwitTextPane extends JTextPane {

	private static final long serialVersionUID = -1642225611560684173L;

	public TwitTextPane() {
		super();
		init();
	}

	public TwitTextPane(String text) {
		super();
		init();

		setText(text);
	}
	
	protected void init() {
		setBorder(new TwitTextPanelBorder(Color.GRAY, 0));
		setStyledDocument(new TwitStyledDocument());
	}
	
	class TwitStyledDocument extends DefaultStyledDocument {
		
		private static final long serialVersionUID = 4306342996658558974L;

		@Override
	    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
	        if ((getLength() + str.length()) <= Constants.TWIT_MAX_CHARACTERS) {
	            super.insertString(offs, str, a);
	        }
	        else {
	            Toolkit.getDefaultToolkit().beep();
	        }
	    }
	}

}
