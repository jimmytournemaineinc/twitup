package com.jandj.twitup.bundles.user.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jandj.twitup.bundles.user.event.LogoutEvent;
import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.LogoutView;

public class LogoutController implements ILogoutController {

	private static final Logger LOGGER = Logger.getLogger(LogoutController.class);
	
	protected LogoutView view;
	protected List<ILogUserListener> listeners;

	public LogoutController() {
		listeners = new ArrayList<>();
	}

	@Override
	public void logout() {
		LOGGER.debug("User logged out");
		listeners.forEach(listener -> listener.loggedOut(new LogoutEvent()));
	}

	@Override
	public LogoutView getView() {
		return view;
	}

	@Override
	public void setView(LogoutView view) {
		this.view = view;
	}

	@Override
	public void addListener(ILogUserListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(ILogUserListener listener) {
		listeners.remove(listener);
	}

}
