package com.jandj.twitup.bundles.user.control;

import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.LoginView;
import com.jandj.twitup.bundles.user.ui.LogoutView;
import com.jandj.twitup.bundles.user.ui.SecurityView;
import com.jandj.twitup.bundles.user.ui.SigninView;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;

public class SecurityController {

	protected EntityManager em;
	protected IDatabase db;
	
	protected SecurityView view;
	protected LogoutView logoutView;
	private ILoginController loginController;
	private ISigninController signinController;
	private ILogoutController logoutController;

	public SecurityController(EntityManager em, IDatabase db) {
		this.em = em;
		this.db = db;
		
		/* Low level UI */
		loginController = new LoginController(em, db);
		signinController = new SigninController(em, db);
		logoutController = new LogoutController();

		SigninView signinView = new SigninView(signinController);
		LoginView loginView = new LoginView(loginController);
		logoutView = new LogoutView(logoutController);

		loginController.setView(loginView);
		signinController.setView(signinView);
		logoutController.setView(logoutView);
		
		/* Middle level UI */
		view = new SecurityView();
		view.setLoginView(loginView);
		view.setSigninView(signinView);
		
	}
	
	public SecurityView getSecurityView() {
		return view;
	}

	public LogoutView getLogoutView() {
		return logoutView;
	}

	public void addLogListener(ILogUserListener listener) {
		loginController.addListener(listener);
		logoutController.addListener(listener);
		signinController.addListener(listener);
	}
	
	
}
