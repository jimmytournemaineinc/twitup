package com.jandj.twitup.bundles.user.event;

import com.jandj.twitup.datamodel.model.User;

public class LoginEvent extends UserEvent {

	public LoginEvent(User user) {
		super(user);
	}

}
