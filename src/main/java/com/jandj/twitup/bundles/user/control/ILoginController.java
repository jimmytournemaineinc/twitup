package com.jandj.twitup.bundles.user.control;

import javax.swing.JPanel;

import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.LoginView;
import com.jandj.twitup.control.Controller;
import com.jandj.twitup.events.Listenable;

/**
 * Define the login controls.
 * 
 * @author Jimmy Tournemaine
 */
public interface ILoginController extends Controller, Listenable<ILogUserListener> {

	/**
	 * Return the login view.
	 * 
	 * @return the login view.
	 */
	public JPanel getLoginView();

	/**
	 * Log a user.
	 * 
	 * @param username The username for loging.
	 * @param password The password for logging.
	 */
	public void login(String username, String password);

	/**
	 * Set the current view.
	 * 
	 * @param loginView
	 */
	public void setView(LoginView loginView);


}
