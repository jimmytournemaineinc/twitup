package com.jandj.twitup.bundles.user.ui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;

import com.jandj.twitup.bundles.user.control.IUserController;
import com.jandj.twitup.datamodel.model.User;

public class UserProfilView extends JPanel implements ActionListener, IUserProfilView {

	/** Serial Version UID. */
	private static final long serialVersionUID = -2897379233604420948L;

	protected static final String DEFAULT_AVATAR = "images/default-avatar.png";

	private User user;
	protected boolean editable;
	protected JTextField userNameField;
	protected JTextField userTagField;
	protected JTextField passwordField;
	protected transient IUserController controller;

	public UserProfilView(IUserController controller) {
		this.controller = controller;
		setLayout(new GridBagLayout());
		
		refresh();
	}
	
	public void refresh() {
		JPanel avatarPanel = (user == null) ? new JPanel() : createAvatarPanel(user.getAvatarPath());
		JPanel userProfilPanel = (user == null) ? new JPanel() : createUserProfilPanel(user);
		
		add(avatarPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		add(userProfilPanel, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	public void setUser(User user, boolean editable) {
		this.user = user;
		setEnabled(editable);
		refresh();
	}

	private JPanel createAvatarPanel(String avatarPath) {
		JPanel formPanel = new JPanel();

		formPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		formPanel.setLayout(new GridBagLayout());

		formPanel.add(avatarPanel(), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		formPanel.add(buttonPanel(), new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.SOUTH,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		return formPanel;

	}

	private JPanel buttonPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		buttonPanel.setLayout(new GridBagLayout());

		JButton validationButton = new JButton("Edit avatar");
		// TODO Avatar edition
		//validationButton.addActionListener(e -> controller.editAvatar());

		buttonPanel.add(validationButton, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTH,
				GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 10), 0, 0));
		return buttonPanel;
	}

	private JPanel avatarPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.GREEN);
		return buttonPanel;
	}

	private JPanel createUserProfilPanel(User user) {
		JPanel formPanel = new JPanel();
		formPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		formPanel.setLayout(new GridBagLayout());

		JLabel userNameLabel = new JLabel("Nom d'utilisateur", SwingConstants.RIGHT);
		userNameField = new JTextField();
		userNameField.setText(user.getName());

		JLabel userTagLabel = new JLabel("Tag de l'utilisateur", SwingConstants.RIGHT);
		userTagField = new JTextField();
		userTagField.setText(user.getUserTag());

		JLabel passwordLabel = new JLabel("Mot de passe", SwingConstants.RIGHT);
		passwordField = new JTextField();
		passwordField.setText(user.getUserPassword());

		JButton validationButton = new JButton("Edit profil");
		validationButton.addActionListener(this);

		formPanel.add(userNameLabel, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(userNameField, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 40), 0, 0));

		formPanel.add(userTagLabel, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(userTagField, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 40), 0, 0));

		formPanel.add(passwordLabel, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(passwordField, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 40), 0, 0));

		formPanel.add(validationButton, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		return formPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!isValid(userNameField)) {
			JOptionPane.showMessageDialog(null, "Le nom de l'utilisateur ne doit pas être nul");
		} else if (!isValid(userTagField)) {
			JOptionPane.showMessageDialog(null, "Le tag de l'utilisateur ne doit pas être nul");
		} else if (!isValid(passwordField)) {
			JOptionPane.showMessageDialog(null, "Le mot de passe de l'utilisateur ne doit pas être nul");
		} else {
			controller.editUserProfil(userTagField.getText(), passwordField.getText(), userNameField.getText());
		}
	}

	protected boolean isValid(JTextField component) {
		return StringUtils.isNotBlank(component.getText());
	}

}
