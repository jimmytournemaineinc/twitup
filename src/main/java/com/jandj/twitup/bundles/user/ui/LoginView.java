package com.jandj.twitup.bundles.user.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.jandj.twitup.bundles.user.control.ILoginController;

public class LoginView extends JPanel {

	private static final long serialVersionUID = -5877768655610198466L;

	protected JTextField userNameField;
	protected JTextField passwordField;

	public LoginView(ILoginController controller) {

		setLayout(new GridBagLayout());
		JPanel formPanel = createFormPanel();
		JPanel buttonsPanel = createButtonsPanel(controller);

		add(formPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		add(buttonsPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
	}

	public JPanel createFormPanel() {
		JPanel formPanel = new JPanel();
		formPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		formPanel.setLayout(new GridBagLayout());

		JLabel userNameLabel = new JLabel("Nom d'utilisateur", SwingConstants.RIGHT);
		userNameField = new JTextField();

		JLabel passwordLabel = new JLabel("Mot de passe", SwingConstants.RIGHT);
		passwordField = new JTextField();

		formPanel.add(userNameLabel, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(userNameField, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		formPanel.add(passwordLabel, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(passwordField, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		return formPanel;

	}

	public JPanel createButtonsPanel(ILoginController controller) {
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		buttonsPanel.setLayout(new GridBagLayout());

		JButton validationButton = new JButton("Submit");
		validationButton.addActionListener(e -> controller.login(userNameField.getText(), passwordField.getText()));

		buttonsPanel.add(validationButton, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));

		return buttonsPanel;

	}

}
