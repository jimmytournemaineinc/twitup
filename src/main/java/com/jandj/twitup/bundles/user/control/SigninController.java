package com.jandj.twitup.bundles.user.control;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.swing.JOptionPane;

import com.jandj.twitup.bundles.user.event.LoginEvent;
import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.SigninView;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.IDatabaseObserver;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.datamodel.model.User;

public class SigninController implements ISigninController, IDatabaseObserver {

	protected EntityManager em;
	protected List<User> listUsers;
	protected SigninView view;
	protected List<ILogUserListener> listeners;
	protected IDatabase db;

	public SigninController(EntityManager em, IDatabase db) {
		this.em = em;
		this.listUsers = new ArrayList<>();
		this.listeners = new ArrayList<>();
		this.db = db;
		
		this.db.addObserver(this);
	}

	public boolean isUserTagAlreadyExist(String userTag) {
		return db.isUserTagExists(userTag);
	}

	@Override
	public void signin(String userTagField, String passwordField, String userNameField) {
		if (!isUserTagAlreadyExist(userTagField)) {
			User user = new User(UUID.randomUUID(), userTagField, passwordField, userNameField, new HashSet<String>(),
					"");
			//db.addUser(user);
			em.sendUser(user);
			
			for (ILogUserListener listener : listeners) {
				listener.loggedIn(new LoginEvent(user));
			}
		} else {
			JOptionPane.showMessageDialog(null, "You tag name already exists.");
		}
	}

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		// NOT USED
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		// NOT USED
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		// NOT USED

	}

	@Override
	public void notifyUserAdded(User addedUser) {
		listUsers.add(addedUser);
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		listUsers.remove(deletedUser);
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		// NOT USED
	}

	@Override
	public void setView(SigninView view) {
		this.view = view;
	}

	@Override
	public void addListener(ILogUserListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(ILogUserListener listener) {
		listeners.remove(listener);
	}

}
