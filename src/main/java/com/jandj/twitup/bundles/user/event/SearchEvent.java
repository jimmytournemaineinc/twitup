package com.jandj.twitup.bundles.user.event;

import com.jandj.twitup.datamodel.model.User;

/**
 * Event announced when a user has been searched.
 */
public class SearchEvent extends UserEvent {

	public SearchEvent(User user) {
		super(user);
	}
}
