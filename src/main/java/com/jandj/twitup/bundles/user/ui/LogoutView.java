package com.jandj.twitup.bundles.user.ui;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import com.jandj.twitup.bundles.user.control.ILogoutController;

public class LogoutView extends JButton {

	/** Serial Version UID */
	private static final long serialVersionUID = -2383682564473179082L;
	
	public LogoutView(ILogoutController controller) {

		super("logout");
		setLayout(new GridBagLayout());
		addActionListener(e -> controller.logout());
	}
}
