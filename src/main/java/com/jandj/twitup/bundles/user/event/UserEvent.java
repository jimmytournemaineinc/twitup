package com.jandj.twitup.bundles.user.event;

import com.jandj.twitup.datamodel.model.User;

public abstract class UserEvent {

	protected User user;
	
	public UserEvent(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
}
