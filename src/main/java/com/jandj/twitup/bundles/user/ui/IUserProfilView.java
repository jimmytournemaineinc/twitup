package com.jandj.twitup.bundles.user.ui;

import com.jandj.twitup.datamodel.model.User;
import com.jandj.twitup.ui.View;

public interface IUserProfilView extends View {

	/**
	 * Set the user to display the profile.
	 * 
	 * @param user
	 */
	public void setUser(User user, boolean editable);
}
