package com.jandj.twitup.bundles.user.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.jandj.twitup.ui.View;

public class SecurityView extends JPanel implements View {

	private static final long serialVersionUID = 747753372206910968L;

	private JPanel loginView;
	private JPanel signinView;
	
	private JPanel loginPane;
	private JPanel signinPane;

	public SecurityView() {
		setLayout(new GridBagLayout());
		loginView = new JPanel();
		signinView = new JPanel();
		
		createLoginPane();
		createSigninPane();
	}
	
	protected void createLoginPane() {
		loginPane = new JPanel();
		loginPane.setLayout(new BorderLayout());
		loginPane.setBorder(new TitledBorder("Already have an account ?"));
	}

	protected void createSigninPane() {
		signinPane = new JPanel();
		signinPane.setLayout(new BorderLayout());
		signinPane.setBorder(new TitledBorder("First visit ? Create an account !"));
	}
	
	protected void refresh() {
		loginPane.add(loginView, BorderLayout.CENTER);
		signinPane.add(signinView, BorderLayout.CENTER);
		
		add(loginPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		add(signinPane, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		
		revalidate();
	}

	public void setLoginView(LoginView view) {
		loginView = view;
		refresh();
	}

	public void setSigninView(SigninView view) {
		signinView = view;
		refresh();
	}

}
