package com.jandj.twitup.bundles.user.control;

import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.SigninView;
import com.jandj.twitup.control.Controller;
import com.jandj.twitup.events.Listenable;

public interface ISigninController extends Controller, Listenable<ILogUserListener> {


	/**
	 * Log the user if the user exists and the password matching the password in the
	 * database corresponding to the user name.
	 * 
	 * @param userTag
	 *            The user's tag.
	 * @param password
	 *            The user's password
	 * @param userName
	 *            The user's name.
	 */
	public void signin(String userTag, String password, String userName);

	/**
	 * Set the view of the sign in controller.
	 * 
	 * @param view The view.
	 */
	public void setView(SigninView view);
	
}
