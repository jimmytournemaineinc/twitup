package com.jandj.twitup.bundles.user.control;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.jandj.twitup.bundles.user.event.SearchEvent;
import com.jandj.twitup.bundles.user.listener.ISearchUserListener;
import com.jandj.twitup.bundles.user.ui.ISearchView;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.model.User;

public class SearchController implements ISearchController {
	
	private static final Logger LOGGER = Logger.getLogger(SearchController.class);
	
	protected EntityManager em;
	protected ISearchView view;
	protected ArrayList<ISearchUserListener> listeners;

	public SearchController(EntityManager em, ISearchView view) {
		this.em = em;
		this.view = view;
		this.listeners = new ArrayList<>();
	}

	@Override
	public void search() {
		User user = view.getSelectedUser();
		
		if(user != null) {
			LOGGER.debug("Announce a new search completed with user "+user.getUserTag());
			
			for (ISearchUserListener listener : listeners) {
				listener.search(new SearchEvent(user));
			}
		}
	}

	@Override
	public void addListener(ISearchUserListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(ISearchUserListener listener) {
		listeners.remove(listener);
		
	}

	@Override
	public ISearchView getSearchView() {
		return view;
	}
	

}
