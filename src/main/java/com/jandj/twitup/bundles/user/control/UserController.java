package com.jandj.twitup.bundles.user.control;

import javax.swing.JOptionPane;

import com.jandj.twitup.bundles.user.listener.ISearchUserListener;
import com.jandj.twitup.bundles.user.ui.ISearchView;
import com.jandj.twitup.bundles.user.ui.IUserProfilView;
import com.jandj.twitup.bundles.user.ui.SearchView;
import com.jandj.twitup.bundles.user.ui.UserProfilView;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.model.User;

public class UserController implements IUserController {

	protected IDatabase db;
	protected EntityManager em;
	
	protected IUserProfilView profileView;
	protected ISearchView searchView;
	
	protected User user;
	private ISearchController searchCtrl;

	public UserController(IDatabase db, EntityManager em) {
		
		this.em = em;
		this.db = db;
		
		searchView = new SearchView(db.getUsers().toArray(new User[0]));
		searchCtrl = new SearchController(em, searchView);
		searchView.setController(searchCtrl);
		
		profileView = new UserProfilView(this);
	}

	@Override
	public void editUserProfil(String userTag, String password, String userName) {
		if(db.isUserTagExists(userTag)) {
			JOptionPane.showMessageDialog(null, "User tag already taken.");
		} else {
			user.setUserTag(userTag);
			user.setUserPassword(password);
			user.setName(userName);
		}
		
		db.modifyUser(user);
	}
	
	@Override
	public void setProfileUser(User user, boolean editable) {
		this.user = user;
		profileView.setUser(user, editable);
	}

	@Override
	public ISearchView getSearchView() {
		return searchView;
	}

	@Override
	public IUserProfilView getProfileView() {
		return profileView;
	}

	@Override
	public void addSearchListener(ISearchUserListener listener) {
		searchCtrl.addListener(listener);
	}
}
