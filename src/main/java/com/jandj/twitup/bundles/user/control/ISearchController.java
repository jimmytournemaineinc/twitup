package com.jandj.twitup.bundles.user.control;

import com.jandj.twitup.bundles.user.listener.ISearchUserListener;
import com.jandj.twitup.bundles.user.ui.ISearchView;
import com.jandj.twitup.control.Controller;
import com.jandj.twitup.events.Listenable;

public interface ISearchController extends Controller, Listenable<ISearchUserListener> {


	/**
	 * Return the search view.
	 * 
	 * @return the search view.
	 */
	public ISearchView getSearchView();
	
	/**
	 * Set the current view.
	 */
	void search();

}
