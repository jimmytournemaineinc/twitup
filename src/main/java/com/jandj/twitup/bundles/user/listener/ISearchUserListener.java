package com.jandj.twitup.bundles.user.listener;

import com.jandj.twitup.bundles.user.event.SearchEvent;

public interface ISearchUserListener {

	/**
	 * A user has been searched.
	 * 
	 * @param e The event containing the user.
	 */
	public void search(SearchEvent e);
}
