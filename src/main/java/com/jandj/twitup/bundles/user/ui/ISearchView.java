package com.jandj.twitup.bundles.user.ui;

import com.jandj.twitup.bundles.user.control.ISearchController;
import com.jandj.twitup.datamodel.model.User;

public interface ISearchView {

	/**
	 * Return the selected user.
	 * 
	 * @return The selected user of null if anything is selected.
	 */
	public User getSelectedUser();

	/**
	 * Set controller.
	 * 
	 * @param controller The controller of the view.
	 */
	public void setController(ISearchController controller);
	
}
