package com.jandj.twitup.bundles.user.control;

import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.LogoutView;
import com.jandj.twitup.control.Controller;
import com.jandj.twitup.events.Listenable;

public interface ILogoutController extends Controller, Listenable<ILogUserListener> {

	public void logout();

	LogoutView getView();

	void setView(LogoutView view);
	
}
