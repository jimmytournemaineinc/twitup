package com.jandj.twitup.bundles.user.control;

import com.jandj.twitup.bundles.user.listener.ISearchUserListener;
import com.jandj.twitup.bundles.user.ui.ISearchView;
import com.jandj.twitup.bundles.user.ui.IUserProfilView;
import com.jandj.twitup.control.Controller;
import com.jandj.twitup.datamodel.model.User;

/**
 * Define the user businesses.
 * 
 * @author Jimmy Tournemaine
 */
public interface IUserController extends Controller {

	/**
	 * Get the search view.
	 * 
	 * @return The search view.
	 */
	public ISearchView getSearchView();

	/**
	 * Get the profile view.
	 * 
	 * @return the profile view.
	 */
	public void editUserProfil(String userTag, String password, String userName);

	/**
	 * Get the profile view
	 * 
	 * @return The profile view.
	 */
	public IUserProfilView getProfileView();

	/**
	 * Set the current user of profile page.
	 * 
	 * @param user
	 */
	public void setProfileUser(User user, boolean editable);

	/**
	 * Add listener to user research.
	 * @param mainController
	 */
	public void addSearchListener(ISearchUserListener listener);

}
