package com.jandj.twitup.bundles.user.control;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.jandj.twitup.bundles.user.event.LoginEvent;
import com.jandj.twitup.bundles.user.listener.ILogUserListener;
import com.jandj.twitup.bundles.user.ui.LoginView;
import com.jandj.twitup.core.EntityManager;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.IDatabaseObserver;
import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.datamodel.model.User;

public class LoginController implements ILoginController, IDatabaseObserver {

	protected EntityManager em;
	protected ArrayList<User> listUsersName;
	protected LoginView loginView;
	protected ArrayList<ILogUserListener> listeners;

	public LoginController(EntityManager em, IDatabase db) {
		this.em = em;
		this.listUsersName = new ArrayList<>();
		this.listeners = new ArrayList<>();

		db.addObserver(this);
	}

	@Override
	public void login(String username, String password) {

		/* Search for a matching user */
		User user = null;
		for (User u : listUsersName) {
			if (username.equals(u.getName())) {
				user = u;
				break;
			}
		}

		/* Unknown username */
		if (user == null) {
			JOptionPane.showMessageDialog(loginView, "This username does not match a known user of the system.",
					"Unknown user", JOptionPane.WARNING_MESSAGE);
			return;
		}

		/* Check password */
		if (!password.equals(user.getUserPassword())) {
			JOptionPane.showMessageDialog(loginView, "The entered password does not match the username.",
					"Wrong password", JOptionPane.WARNING_MESSAGE);
			return;
		}

		/* Diffuse the login event */
		for (ILogUserListener listener : listeners) {
			listener.loggedIn(new LoginEvent(user));
		}

	}

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		// NOT USED
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		// NOT USED
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		// NOT USED
	}

	@Override
	public void notifyUserAdded(User addedUser) {
		listUsersName.add(addedUser);
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		listUsersName.remove(deletedUser);
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		// NOT USED
	}

	@Override
	public void addListener(ILogUserListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(ILogUserListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void setView(LoginView loginView) {
		this.loginView = loginView;
	}

	@Override
	public JPanel getLoginView() {
		return loginView;
	}
}
