package com.jandj.twitup.bundles.user.ui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jandj.twitup.bundles.user.control.ISearchController;
import com.jandj.twitup.datamodel.model.User;

/**
 * View to search user
 */
public class SearchView extends JPanel implements ISearchView {

	/** Serial Version UID */
	private static final long serialVersionUID = 3793758718068488955L;
	private static final Logger LOGGER = Logger.getLogger(SearchView.class);

	protected transient ISearchController controller;
	protected JTextField txtField;
	protected JComboBox<User> cmbSearch;
	protected JPanel jPanel1;
	private boolean hideFlag = false;

	public SearchView(User[] data) {
		initComponents();

		cmbSearch.setToolTipText("Search By Name or Phone Number");
		cmbSearch.setRenderer(new UserListCellRenderer());

		txtField = (JTextField) cmbSearch.getEditor().getEditorComponent();
		txtField.addKeyListener(new Autocomplete(data));

		setModel(new CustomComboBoxModel(data), "");
	}

	private void setModel(CustomComboBoxModel mdl, String str) {
		cmbSearch.setModel(mdl);
		cmbSearch.setSelectedIndex(-1);
		txtField.setText(str);
	}

	@Override
	public void setController(ISearchController controller) {
		this.controller = controller;
	}

	private void initComponents() {

		jPanel1 = new JPanel();
		cmbSearch = new JComboBox<>();

		cmbSearch.setEditable(true);
		cmbSearch.setToolTipText("Search by user name");
		cmbSearch.addActionListener(e -> cmbSearchActionPerformed());

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout
						.createSequentialGroup().addGap(57, 57, 57).addComponent(cmbSearch,
								javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(87, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout
						.createSequentialGroup().addGap(77, 77, 77).addComponent(cmbSearch,
								javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(190, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		revalidate();
	}

	private void cmbSearchActionPerformed() {
		if (controller == null) {
			LOGGER.warn("There is no controller to handle this view.");
		} else {
			controller.search();
		}

		User u = getSelectedUser();
		if (u != null) {
			txtField.setText('@' + u.getUserTag());
		}
	}

	@Override
	public User getSelectedUser() {
		return (User) cmbSearch.getSelectedItem();
	}

	class Autocomplete extends KeyAdapter {

		User[] data;

		Autocomplete(User[] data) {
			this.data = data;
		}

		@Override
		public void keyTyped(KeyEvent e) {
			EventQueue.invokeLater(() -> {
				String text = txtField.getText();
				if (text.length() == 0) {
					cmbSearch.hidePopup();
					setModel(getSuggestedModel(data, ""), "");
				} else {
					CustomComboBoxModel m = getSuggestedModel(data, text);
					if (m.getSize() == 0 || hideFlag) {
						cmbSearch.hidePopup();
						hideFlag = false;
					} else {
						setModel(m, text);
						cmbSearch.showPopup();
					}
				}
			});
		}

		@Override
		public void keyPressed(KeyEvent e) {
			int code = e.getKeyCode();
			if (code == KeyEvent.VK_ESCAPE) {
				hideFlag = true;
			}
		}

		private CustomComboBoxModel getSuggestedModel(User[] list, String text) {
			CustomComboBoxModel cmbModel = new CustomComboBoxModel();
			cmbModel.removeAllElements();
			for (User user : list) {
				if (StringUtils.containsIgnoreCase(user.getName(), text)
						|| StringUtils.containsIgnoreCase('@' + user.getUserTag(), text)) {
					cmbModel.addElement(user);
				}
			}
			return cmbModel;
		}
	}

	class CustomComboBoxModel extends DefaultComboBoxModel<User> {

		/** Serial Version UID. */
		private static final long serialVersionUID = 8973886701086098624L;

		public CustomComboBoxModel() {
			super();
		}

		public CustomComboBoxModel(User[] items) {
			super(items);
		}

		@Override
		public User getSelectedItem() {
			Object o = super.getSelectedItem();
			if(o != null && o instanceof User) {
				return (User) super.getSelectedItem();
			}
			return null;
		}
	}

	public class UserListCellRenderer extends DefaultListCellRenderer {

		/** Serial Version UID. */
		private static final long serialVersionUID = 6029635178812717574L;

		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {

			if (!(value instanceof User)) {
				throw new IllegalArgumentException();
			}

			User u = (User) value;
			value = u.getName() + " - @" + u.getUserTag();

			return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		}

	}
}
