package com.jandj.twitup.bundles.user.listener;

import com.jandj.twitup.bundles.user.event.LoginEvent;
import com.jandj.twitup.bundles.user.event.LogoutEvent;

/**
 * Listener on login/logout events
 * 
 * @author Jimmy Tournemaine
 */
public interface ILogUserListener {

	/**
	 * A user is now logged in.
	 * 
	 * @param e The event.
	 */
	public void loggedIn(LoginEvent e);

	/**
	 * A user is now logged out.
	 * 
	 * @param e The event.
	 */
	public void loggedOut(LogoutEvent e);
}
