package com.jandj.twitup.bundles.user.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;

import com.jandj.twitup.bundles.user.control.ISigninController;

public class SigninView extends JPanel implements ActionListener {

	private static final long serialVersionUID = 6621366434422019732L;
	protected JTextField userNameField;
	protected JTextField userTagField;
	protected JTextField passwordField;
	protected transient ISigninController controller;

	public SigninView(ISigninController controller) {
		this.controller = controller;

		setLayout(new GridBagLayout());
		JPanel formPanel = createFormPanel();
		JPanel buttonsPanel = createButtonsPanel();
		add(formPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		add(buttonsPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
	}

	public JPanel createFormPanel() {
		JPanel formPanel = new JPanel();
		formPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		formPanel.setLayout(new GridBagLayout());

		JLabel userNameLabel = new JLabel("Nom d'utilisateur", SwingConstants.RIGHT);
		userNameField = new JTextField();

		JLabel userTagLabel = new JLabel("Tag de l'utilisateur", SwingConstants.RIGHT);
		userTagField = new JTextField();

		JLabel passwordLabel = new JLabel("Mot de passe", SwingConstants.RIGHT);
		passwordField = new JTextField();

		formPanel.add(userNameLabel, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(userNameField, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		formPanel.add(userTagLabel, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(userTagField, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		formPanel.add(passwordLabel, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
		formPanel.add(passwordField, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		return formPanel;

	}

	public JPanel createButtonsPanel() {
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		buttonsPanel.setLayout(new GridBagLayout());

		JButton validationButton = new JButton("Submit");
		validationButton.addActionListener(this);

		buttonsPanel.add(validationButton, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));

		return buttonsPanel;

	}

	public void actionPerformed(ActionEvent e) {
		if (!isValid(userNameField)) {
			JOptionPane.showMessageDialog(null, "Le nom de l'utilisateur ne doit pas être nul");
		} else if (!isValid(userTagField)) {
			JOptionPane.showMessageDialog(null, "Le tag de l'utilisateur ne doit pas être nul");
		} else {
			controller.signin(userTagField.getText(), passwordField.getText(), userNameField.getText());
		}
	}

	public boolean isValid(JTextField component) {
		return StringUtils.isNotBlank(component.getText());
	}

}
