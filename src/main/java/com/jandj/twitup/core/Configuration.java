package com.jandj.twitup.core;

import java.util.Properties;

import com.jandj.twitup.common.Constants;
import com.jandj.twitup.common.PropertiesManager;
import com.jandj.twitup.ui.UIMode;

public class Configuration {

	private static Configuration configuration;
	
	private Properties properties;

	private Configuration() {
		PropertiesManager pm = new PropertiesManager();
		
		properties = pm.loadProperties(
				Thread.currentThread().getContextClassLoader().getResource(Constants.CONFIGURATION_FILE).getFile());
	}

	public static Configuration getInstance() {
		if (configuration == null) {
			configuration = new Configuration();
		}
		return configuration;
	}

	public String getExchangeDirectory() {
		return properties.getProperty(Constants.CONFIGURATION_KEY_EXCHANGE_DIRECTORY, "");
	}

	public String getUiClassName() {
		return properties.getProperty(Constants.CONFIGURATION_KEY_UI_CLASS_NAME, "");
	}

	public UIMode getUIMode() {
		return Enum.valueOf(UIMode.class, properties.getProperty(Constants.CONFIGURATION_KEY_UI_MODE, Constants.CONFIGURATION_DEFAULT_UI_MODE));
	}

}
