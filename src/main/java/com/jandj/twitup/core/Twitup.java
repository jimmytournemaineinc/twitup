package com.jandj.twitup.core;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jandj.twitup.bundles.main.control.MainController;
import com.jandj.twitup.datamodel.Database;
import com.jandj.twitup.datamodel.DatabaseObserver;
import com.jandj.twitup.datamodel.IDatabase;
import com.jandj.twitup.datamodel.IDatabaseObserver;
import com.jandj.twitup.events.file.IWatchableDirectory;
import com.jandj.twitup.events.file.WatchableDirectory;
import com.jandj.twitup.ui.DirectoryChooser;
import com.jandj.twitup.ui.UIMode;

import javafx.embed.swing.JFXPanel;

/**
 * Classe principale l'application.
 * 
 * @author S.Lucas
 * @author jimmytournemaine
 */
public class Twitup {

	private static final Logger LOGGER = Logger.getLogger(Twitup.class);

	/**
	 * Base de données.
	 */
	protected IDatabase mDatabase;

	/**
	 * Observateur de la base de données.
	 */
	protected IDatabaseObserver observer;

	/**
	 * Gestionnaire des entités contenu de la base de données.
	 */
	protected EntityManager mEntityManager;

	/**
	 * Vue principale de l'application.
	 */
	protected MainController mMainController;

	/**
	 * Classe de surveillance de répertoire
	 */
	protected IWatchableDirectory mWatchableDirectory;

	/**
	 * Répertoire d'échange de l'application.
	 */
	protected String mExchangeDirectoryPath;

	/**
	 * Nom de la classe de l'UI.
	 */
	protected String mUiClassName;

	/**
	 * Permet de récupérer si l'OS est Mac OS X afin de gérer les spécificités.
	 * 
	 * @return Un booléen indiquant si l'OS est Mac.
	 */
	public static boolean isMac() {
		return System.getProperty("os.name").startsWith("Mac");
	}

	/**
	 * Constructeur.
	 */
	public Twitup() {

		// Init du look and feel de l'application
		this.initLookAndFeel();

		// Initialisation de la base de données
		this.initDatabase();

		// Initialisation du répertoire d'échange
		this.initDirectory();

		// Initialisation de l'IHM
		this.initGui();
	}

	/**
	 * Initialisation du look and feel de l'application.
	 * 
	 * SRS-TWP-APP-03
	 */
	protected void initLookAndFeel() {

		if (isMac()) {
			System.setProperty("apple.laf.useScreenMenuBar", "true");
		}
		initLookAndFeelFromConfiguration();

	}

	protected void initLookAndFeelFromConfiguration() {
		String uiClass = Configuration.getInstance().getUiClassName();
		if (StringUtils.isBlank(uiClass)) {
			setDefaultLookAndFeel();
		} else {
			try {
				UIManager.setLookAndFeel(uiClass);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e) {
				LOGGER.error("Cannot set look & feel : " + uiClass, e);
				setDefaultLookAndFeel();
			}
		}
	}

	protected void setDefaultLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			LOGGER.error(e);
		}
	}

	/**
	 * Initialisation de l'interface graphique.
	 */
	protected void initGui() {
		if (UIMode.JFX == Configuration.getInstance().getUIMode()) {
			EventQueue.invokeLater(JFXPanel::new);
		}

		EventQueue.invokeLater(() -> new MainController(mDatabase, mEntityManager));
	}

	/**
	 * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
	 * chooser). <br/>
	 * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
	 * pouvoir utiliser l'application</b>
	 */
	protected void initDirectory() {

		// SRS-TWP-APP-001
		File directory = new File(Configuration.getInstance().getExchangeDirectory());

		// SRS-TWP-APP-002
		while (null == directory || !isValideExchangeDirectory(directory)) {
			LOGGER.warn("Le répertoire sélectionné est invalide");
			directory = selectDirectoryUI();
		}

		initDirectory(directory.getPath());
	}

	/**
	 * Affiche une fenêtre de sélection d'un répertoire.
	 * 
	 * @return File|null Le repertoire sélectionné.
	 */
	private File selectDirectoryUI() {

		File directory = null;
		try (DirectoryChooser chooser = new DirectoryChooser("Choix du répertoire d'échanges")) {
			if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				directory = chooser.getSelectedFile();
			} else {
				LOGGER.debug("Configuration du répertoire d'échanges avortée.");
			}
		}
		return directory;
	}

	/**
	 * Indique si le fichier donné est valide pour servire de répertoire d'échange
	 * 
	 * @param directory
	 *            , Répertoire à tester.
	 */
	protected boolean isValideExchangeDirectory(File directory) {
		// Valide si répertoire disponible en lecture et écriture
		return directory.exists() && directory.isDirectory() && directory.canRead() && directory.canWrite();
	}

	/**
	 * Initialisation de la base de données
	 */
	protected void initDatabase() {
		mDatabase = new Database();
		mEntityManager = new EntityManager(mDatabase);
		observer = new DatabaseObserver();

		mDatabase.addObserver(observer);
	}

	/**
	 * Initialisation du répertoire d'échange.
	 * 
	 * @param directoryPath
	 */
	public void initDirectory(String directoryPath) {
		mExchangeDirectoryPath = directoryPath;
		mWatchableDirectory = new WatchableDirectory(directoryPath);
		mEntityManager.setExchangeDirectory(directoryPath);

		mWatchableDirectory.initWatching();
		mWatchableDirectory.addObserver(mEntityManager);
	}
}
