package com.jandj.twitup.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Classe utilitaire de gestion du chargement et de la sauvegarde des
 * configuration.
 * 
 * @author S.Lucas
 */
public class PropertiesManager {

	private static final Logger LOGGER = Logger.getLogger(PropertiesManager.class);

	/**
	 * Chargement des propriétés de configuration de l'application (depuis un
	 * éventuel fichier de configuration).
	 */
	public Properties loadProperties(String configurationFilePath) {
		Properties properties = new Properties();

		// Si le fichier de configuration existe
		if (new File(configurationFilePath).exists()) {

			try (FileInputStream in = new FileInputStream(configurationFilePath)) {
				properties.load(in);
			} catch (IOException e) {
				LOGGER.error("Impossible de charger les configurations", e);
			}
		}

		return properties;
	}

	/**
	 * Ecriture du fichier de configuration.
	 * 
	 * @param properties Configurations enregistrées
	 * @param configurationFilePath Chemin du fichier de configuration à écrire.
	 */
	public void writeProperties(Properties properties, String configurationFilePath) {
		if (properties != null) {

			try (FileOutputStream out = new FileOutputStream(configurationFilePath)) {
				properties.store(out, "Set up TwitUp! app.");
			} catch (IOException e) {
				LOGGER.error("System is unable to write configuration properties.", e);
			}
		}
	}
}
