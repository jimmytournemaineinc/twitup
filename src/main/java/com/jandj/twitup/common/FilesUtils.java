package com.jandj.twitup.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;

import org.apache.log4j.Logger;

/**
 * Classe utilitaire pour la gestion des fichiers.
 * 
 * @author S.Lucas
 */
public class FilesUtils {

	private static final Logger LOGGER = Logger.getLogger(FilesUtils.class);

	private FilesUtils() {
		super();
	}

	/**
	 * Déplacement du fichier source vers le fichier destination.
	 * 
	 * @param sourceFileName
	 *            , Chemin du fichier source
	 * @param destFileName
	 *            , Chemin du fichier de destination
	 * @return un booléen indiquant si le déplacement s'est déroulé avec succès.
	 */
	public static boolean moveFile(File sourceFile, String destFileName) {

		// Copie du fichier
		 boolean isOk = copyFile(sourceFile.getAbsolutePath(), destFileName);

		// Si c'est bon, suppression de la source
		if (isOk) {
			try {
				Files.delete(sourceFile.toPath());
			} catch (IOException e) {
				LOGGER.error("Cannot delete " + sourceFile, e);
				isOk = false;
			}
		}

		return isOk;
	}

	/**
	 * Déplacement du fichier source vers le fichier destination.
	 * 
	 * @param sourceFileName
	 *            Chemin du fichier source
	 * @param destFileName
	 *            Chemin du fichier de destination
	 * @return un booleen indiquant si la copie s'est déroulée avec succès.
	 */
	public static boolean copyFile(String sourceFileName, String destFileName) {
		boolean isOk = false;

		try (FileInputStream fis = new FileInputStream(sourceFileName);
				// Init
				FileChannel in = fis.getChannel();
				FileOutputStream fos = new FileOutputStream(destFileName);
				FileChannel out = fos.getChannel();) {
			// Transfert des contenus
			in.transferTo(0, in.size(), out);

			isOk = true;
		} catch (Exception e) {
			LOGGER.error("Erreur lors de la copie du fichier '" + sourceFileName + "'", e);
		}

		return isOk;
	}
}
