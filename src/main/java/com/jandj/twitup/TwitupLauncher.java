package com.jandj.twitup;

import com.jandj.twitup.core.Twitup;

/**
 * Classe de lancement de l'application.
 * 
 * @author S.Lucas
 */
public class TwitupLauncher {

	/**
	 * Launcher.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Twitup();
	}

}
