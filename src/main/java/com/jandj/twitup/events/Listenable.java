package com.jandj.twitup.events;

public interface Listenable<T> {

	public void addListener(T listener);
	public void removeListener(T listener);
}
