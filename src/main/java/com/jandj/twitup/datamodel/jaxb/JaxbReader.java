package com.jandj.twitup.datamodel.jaxb;

import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.jandj.twitup.common.Constants;
import com.jandj.twitup.datamodel.jaxb.bean.twit.TwitXml;
import com.jandj.twitup.datamodel.jaxb.bean.user.UserXml;

/**
 * Classe de lecture des fichiers XML.
 * 
 * @author S.Lucas
 */
public class JaxbReader {

	/** Beans root backage */
	protected static final String JAXB_BEAN_ROOT_PACKAGE = "com.jandj.twitup.datamodel.jaxb.bean";

	/** Twit bean package */
	protected static final String JAXB_TWIT_BEAN_PACKAGE = JAXB_BEAN_ROOT_PACKAGE + "." + "twit";

	/** User bean package */
	protected static final String JAXB_USER_BEAN_PACKAGE = JAXB_BEAN_ROOT_PACKAGE + "." + "user";

	/** Logger */
	private static final Logger LOGGER = Logger.getLogger(JaxbReader.class);

	/** Private constructor to hide the default one. */
	private JaxbReader() {
		super();
	}
	
	/**
	 * Lecture du fichier XML pour un {@link TwitXml}
	 * 
	 * @param twitFileName
	 */
	public static TwitXml readTwit(String twitFileName) {
		TwitXml twit = null;

		if (twitFileName != null && twitFileName.endsWith(Constants.TWIT_FILE_EXTENSION)) {
			twit = (TwitXml) readFile(twitFileName, JAXB_TWIT_BEAN_PACKAGE);
		}

		return twit;
	}

	/**
	 * Lecture du fichier XML pour un {@link UserXml}
	 * 
	 * @param twitFileName
	 */
	public static UserXml readUser(String userFileName) {
		UserXml user = null;

		if (userFileName != null && userFileName.endsWith(Constants.USER_FILE_EXTENSION)) {
			user = (UserXml) readFile(userFileName, JAXB_USER_BEAN_PACKAGE);
		}

		return user;
	}

	/**
	 * Unmarshalling XML file
	 * 
	 * @param xmlFileName Fichier XML à lire
	 * @param beanPackage Package contenant les bean JAXB.
	 */
	protected static Object readFile(String xmlFileName, String beanPackage) {
		Object object = null;
		
			try {
				JAXBContext context;
				context = JAXBContext.newInstance(beanPackage);
				Unmarshaller unmarshaller = context.createUnmarshaller();
				object = unmarshaller.unmarshal(new FileReader(xmlFileName));
			} catch (FileNotFoundException | JAXBException e) {
				LOGGER.error("Unable to unmarshall XML file",e);
			}
		
		return object;
	}

}
