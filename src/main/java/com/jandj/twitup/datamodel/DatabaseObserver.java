package com.jandj.twitup.datamodel;

import org.apache.log4j.Logger;

import com.jandj.twitup.datamodel.model.Twit;
import com.jandj.twitup.datamodel.model.User;

/**
 * Observation de la base pour affichage des Twits.
 * 
 * @author jimmytournemaine
 */
public class DatabaseObserver implements IDatabaseObserver {

	private static final Logger LOGGER = Logger.getLogger(DatabaseObserver.class);
	
	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		LOGGER.debug("Twit ajouté : "+addedTwit);
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		LOGGER.debug("Twit supprimé : "+deletedTwit);
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		LOGGER.debug("Twit modifié : "+modifiedTwit);
	}

	@Override
	public void notifyUserAdded(User addedUser) {
		LOGGER.debug("Utilisateur ajouté : "+addedUser);
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		LOGGER.debug("Utilisateur supprimé : "+deletedUser);
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		LOGGER.debug("Utilisateur modifié : "+modifiedUser);
	}

}
