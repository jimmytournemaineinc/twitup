package com.jandj.twitup.ui;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class DirectoryChooser extends JFileChooser implements AutoCloseable {

	private static final long serialVersionUID = 8203586997867631946L;
	
	JFrame frame;

	public DirectoryChooser() {
		this("Select a directory");
	}

	public DirectoryChooser(String title) {
		this(title, ".");
	}

	public DirectoryChooser(String title, String currentDirectory) {
		this.setCurrentDirectory(new File(currentDirectory));
		this.setDialogTitle(title);
		this.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		this.setAcceptAllFileFilterUsed(false);
	}
	
	@Override
	public int showOpenDialog(Component parent) {
		Component owner = (parent == null) ? getFrame() : parent;
		return super.showOpenDialog(owner);
	}

    private JFrame getFrame() {
        if (frame==null) {
            frame = new JFrame();
            frame.setSize(600, 400);
            frame.setLocationRelativeTo(null);
        }
        return frame;
    }

	@Override
	public void close() {
		frame.dispose();
	}
}
