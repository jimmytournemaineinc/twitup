package com.jandj.twitup.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * A panel which contains an image.
 * 
 * @author J. Tournemaine
 */
public class ImagePanel extends JPanel {
	
	/** Serial version UID */
	private static final long serialVersionUID = 792947069109894536L;
	
	/** The original image */
	private transient Image originalImage;
	
	/** The calculated image */
	private transient Image image;

	/** Create an image panel from the URL to an image file */
	public ImagePanel(URL url) throws IOException {
		originalImage = ImageIO.read(new File(url.getPath()));
		image = originalImage;
		addComponentListener(new ResizeListener());
		
		// FIXME First display don't show the image...
		setSize(1000, 1000);
	}
	
	/** Create an image panel from a path accessible on the classpath */
	public ImagePanel(String classpathFilepath) throws IOException {
		this(Thread.currentThread().getContextClassLoader().getResource(classpathFilepath));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int x = getWidth() / 2 - image.getHeight(this) / 2;
		int y = getHeight() / 2 - image.getHeight(this) / 2;
		
		g.drawImage(image, x, y, this);
	}
	
	/**
	 * Listen to the Image Panel resized event to resize the image.
	 * 
	 * @author jimmytournemaine
	 */
	class ResizeListener extends ComponentAdapter {
		
		@Override
        public void componentResized(ComponentEvent e) {
            
			int originalImageWidth = originalImage.getWidth(ImagePanel.this);
			int originalImageHeight = originalImage.getHeight(ImagePanel.this);
			
			int panelWidth = getWidth();
			int panelHeight = getHeight();
			
			if((originalImageWidth > panelWidth || originalImageHeight > panelHeight) && panelWidth != 0 && panelHeight != 0) {
				
				double maxRatio = Math.max(originalImageWidth / panelWidth, originalImageHeight / panelHeight);
				
				int width = (int) Math.round(originalImageWidth / maxRatio);
				int height = (int) Math.round(originalImageHeight / maxRatio);
				
				image = originalImage.getScaledInstance(width, height, Image.SCALE_DEFAULT);
			} else {
				image = originalImage;
			}
        }
	}
}
