package com.jandj.twitup.ui.osx;

import com.jandj.twitup.bundles.main.ui.AboutView;

public class MainViewMenuHandler {
	
	private MainViewMenuHandler() {
		super();
	}
	
	static {
		com.apple.eawt.Application.getApplication().setAboutHandler(ev -> AboutView.showAbout());
	}
}
